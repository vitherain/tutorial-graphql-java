package cz.inventi.tutorial;

import com.oembedler.moon.graphql.engine.stereotype.GraphQLSchema;
import com.oembedler.moon.graphql.engine.stereotype.GraphQLSchemaQuery;

/**
 * Created by Vit Herain on 20/12/2016.
 */
@GraphQLSchema
public class UserSchema {

    @GraphQLSchemaQuery
    private UserQuery root;
}
