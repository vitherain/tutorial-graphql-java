package cz.inventi.tutorial;

import com.oembedler.moon.graphql.engine.stereotype.GraphQLDescription;
import com.oembedler.moon.graphql.engine.stereotype.GraphQLField;
import com.oembedler.moon.graphql.engine.stereotype.GraphQLNonNull;
import com.oembedler.moon.graphql.engine.stereotype.GraphQLObject;

/**
 * Created by Vit Herain on 20/12/2016.
 */
@GraphQLObject("User")
public class UserQuery {

    @GraphQLNonNull
    @GraphQLDescription("Users ID")
    private String id;

    @GraphQLNonNull
    @GraphQLDescription("Users first name")
    private String firstName;

    private String lastName;

    @GraphQLField
    @GraphQLDescription("Users first name")
    public String fullName() {
        return String.format("%s %s", firstName, lastName);
    }

    @GraphQLNonNull
    @GraphQLDescription("Users email")
    private String email;

    @GraphQLNonNull
    @GraphQLDescription("Users phone number")
    private String telephone;

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(final String telephone) {
        this.telephone = telephone;
    }
}
