package cz.inventi.tutorial;

import com.oembedler.moon.graphql.boot.EnableGraphQLServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableGraphQLServer
@SpringBootApplication
public class TutorialGraphqlJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TutorialGraphqlJavaApplication.class, args);
	}
}
